package com.nijjwal.project1;

public class Test1 {

	public void m1() {
		for (int i = 0; i < 10; i++) {
			System.out.println("Child thread " + i + " is running.");
		}
	}

	public static void main(String[] args) {
		// Child thread without using Lambda Expression when method is
		// non-static
		Test1 t1 = new Test1();
		Runnable r = t1::m1;

		Thread t = new Thread(r);
		t.start();
		// Child thread without using Lambda Expression when method is
		// non-static

		for (int i = 0; i < 10; i++) {
			System.out.println("Main thread is running.");
		}
	}

}
