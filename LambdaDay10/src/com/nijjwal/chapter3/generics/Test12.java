package com.nijjwal.chapter3.generics;

//Chapter 3 - Looping through a collection using lambda and method reference

import java.util.List;
import java.util.Arrays;

public class Test12 {
	public static void main(String[] args) {
		// Create a list
		List<String> cats = Arrays.asList("Annie", "Frankie");

		// Print the list using lambda expression
		System.out.println("Print cats name using lambda expression: ");
		cats.forEach(c -> System.out.println(c));

		// Print the list using method reference again
		System.out.println("Print cats name using method reference: ");
		cats.forEach(System.out::println);
	}
}