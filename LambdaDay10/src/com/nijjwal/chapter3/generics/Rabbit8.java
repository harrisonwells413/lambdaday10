/**
 * Chapter 3 - Sorting using TreeSet resulting into ClassCastException, when an object is added to the TreeSet
 */

package com.nijjwal.chapter3.generics;

import java.util.Set;
import java.util.TreeSet;
import java.util.Comparator;

public class Rabbit8 {
	private String name;
	private int id;

	public Rabbit8(String name, int id) {
		this.name = name;
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public int getId() {
		return id;
	}

	public String toString() {
		return name + " id is : " + id;
	}

	public static void main(String... args) {
		/*
		 * Use Comparator to tell TreeSet, one of the collections on how the
		 * objects should be ordered before inserting the first object.
		 */
		Set<Rabbit8> setOfRabbit = new TreeSet<>((r1, r2) -> r1.getId()
				- r2.getId());

		setOfRabbit.add(new Rabbit8("Calvin", 3));
		setOfRabbit.add(new Rabbit8("Henry", 1));

		System.out.println(setOfRabbit);
	}

}