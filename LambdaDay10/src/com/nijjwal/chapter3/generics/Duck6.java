// Chapters 3 - Test 6
package com.nijjwal.chapter3.generics;

import java.util.List;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Using generics so that Duck6 class is sortable. Also, the type specified in
 * the Comparable and compareTo method must match. If type is not provided in
 * Comparable, then compareTo should have type as Object, otherwise code will
 * not compile.
 * 
 * @author Nijjwal
 *
 */
public class Duck6 implements Comparable<Duck6> {
	private String name;
	private int weight;

	// Constructor's purpose is to create an object and initialize instance
	// variables
	public Duck6(String name, int weight) {
		this.name = name;
		this.weight = weight;
	}

	public String getName() {
		return name;
	}

	public int getWeight() {
		return weight;
	}

	public String toString() {
		return name + " weight is: " + weight;
	}

	// For simplicity, not overriding equals & hashcode method
	public int compareTo(Duck6 d) {
		return name.compareTo(d.name);
	}

	public static void main(String[] args) {

		// Comparator using Lambda Expression instead of anonymous inner class to write concise code. 
	    Comparator<Duck6> compareByWeight = (d1, d2) -> d1.getWeight() - d2.getWeight();

		// Remember: Numbers sort before letters & uppercase sort before
		// lowercase letters
		List<Duck6> ducks = new ArrayList<>();
		ducks.add(new Duck6("mike", 13));
		ducks.add(new Duck6("Mike", 10));
		ducks.add(new Duck6("Nancy", 19));
		ducks.add(new Duck6("11", 12));
		ducks.add(new Duck6("jonathan", 21));

		// 1. Sort Duck6s using Comparable interface and by name first.
		Collections.sort(ducks);
		System.out.println("Result of sorting by name: " + ducks);

		// 2. Again, sort Duck6s by weight
		Collections.sort(ducks, compareByWeight);
		System.out.println("Result of sorting by weight: " + ducks);

	}
}
