package com.nijjwal.chapter3.generics;

//Chapter 3 - Removing Conditonally

import java.util.List;
import java.util.ArrayList;

public class Test10 {
	public static void main(String[] args) {
		// Create a list
		List<String> list = new ArrayList<>();
		list.add("Magician");
		list.add("Assistant");

		// Print the list
		System.out.println(list);

		// Remove using removeIf new method # 1
		list.removeIf(s -> s.startsWith("A"));

		// Print the list again
		System.out.println(list);

	}
}