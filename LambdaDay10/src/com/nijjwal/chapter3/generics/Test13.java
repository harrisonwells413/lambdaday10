package com.nijjwal.chapter3.generics;

//Chapter 3 - Updating All Elements

import java.util.Map;
import java.util.HashMap;

public class Test13 {
	public static void main(String[] args) {
		// Create a Map - HashMap - does not remember insertion order and is not
		// sorted
		Map<String, String> dinnerOrder = new HashMap<>();
		dinnerOrder.put("Jenny", "Combo Fried Rice");
		dinnerOrder.put("Tom", null);

		// Use putIfAbsent new method - insert new key/value & doesnot override
		// if value is not null
		dinnerOrder.putIfAbsent("Jenny", "Falafal");
		dinnerOrder.putIfAbsent("Jason", "Falafal");
		dinnerOrder.putIfAbsent("Tom", "Falafal");

		// Print the dinner order
		System.out.println(dinnerOrder);

	}
}