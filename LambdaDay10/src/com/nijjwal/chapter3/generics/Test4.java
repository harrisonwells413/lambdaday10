package com.nijjwal.chapter3.generics;

/* Sample class to display that class cast exception can occur
* when using Generics*/

import java.util.List;
import java.util.ArrayList;

public class Test4{

	public static void main(String... args){
		List names = new ArrayList();
		names.add(new StringBuilder("Henry"));
	}
	
}