package com.nijjwal.chapter3.generics;

//Chapter 3 - Merge function for Map

import java.util.function.BiFunction;
import java.util.Map;
import java.util.HashMap;

public class Test14 {
	public static void main(String[] args) {
		// Mapping function
		BiFunction<String, String, String> mapper = (v1, v2) -> v1.length() > v2
				.length() ? v1 : v2;

		// Create a Map - HashMap - does not remember insertion order and is not
		// sorted
		Map<String, String> favorites = new HashMap<>();
		favorites.put("Jenny", "Bus Tour");
		favorites.put("Tom", "Tram");
		favorites.put("Sam", null);

		// Use merge method to update ride if the length of Skyride is greater
		// than the ride
		// chosen by the rider at the beginning.
		String jenny = favorites.merge("Jenny", "Skyride", mapper);
		String tom = favorites.merge("Tom", "Skyride", mapper);

		// Since there is no value for Sam, so merge will not call the mapper,
		// it will simply
		// assign the new value. This is to avoid NullPointerException.
		String sam = favorites.merge("Sam", "Skyride", mapper);

		// Similarly if the key does not exist merge function simply assigns the
		// new value
		// & doesnot call the mapper function to avoid NullPointerException.
		String henry = favorites.merge("Henry", "Skyrider", mapper);

		// Print the map
		System.out.println(favorites);

		// Print the result of map on each key
		System.out.println("Jenny will go by: " + jenny);
		System.out.println("Tom will go by: " + tom);
		System.out.println("Sam will go by: " + sam);
		System.out.println("Henry will go by: " + henry);

	}
}