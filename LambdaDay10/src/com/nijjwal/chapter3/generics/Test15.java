package com.nijjwal.chapter3.generics;

//Chapter 3 - Merge function with mapper returning null to remove the key if the mapper is called.

import java.util.function.BiFunction;
import java.util.Map;
import java.util.HashMap;

public class Test15 {
	public static void main(String[] args) {
		// Mapping function
		BiFunction<String, String, String> mapper = (v1, v2) -> null;

		/*
		 * Create a map - HashMap - which is neither sorted nor preserves the
		 * insertion order.
		 */
		Map<String, String> favorites = new HashMap<>();
		favorites.putIfAbsent("Jenny", "Bus Tour");
		favorites.putIfAbsent("Tom", "Bus Tour");

		// merge method removes the key if the mapper returns null
		favorites.merge("Jenny", "Skyride", mapper);
		favorites.merge("Sam", "Skyride", mapper);

		// Print the map
		System.out.println(favorites);
	}
}