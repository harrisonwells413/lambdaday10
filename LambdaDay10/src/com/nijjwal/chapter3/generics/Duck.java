// Chapters 3 - Test 5
package com.nijjwal.chapter3.generics;

import java.util.List;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Using generics so that Duck class is sortable. Also, the type specified in
 * the Comparable and compareTo method must match. If type is not provided in
 * Comparable i.e Legacy Code is used, then compareTo should have type as
 * Object, otherwise code will not compile. And in this scenario casting will be
 * required.
 * 
 * @author Nijjwal
 *
 */
public class Duck implements Comparable<Duck> {
	private String name;

	// Constructor's purpose is to create an object and initialize instance
	// variables
	public Duck(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public String toString() {
		return name;
	}

	// For simplicity, not overriding equals & hashcode method
	public int compareTo(Duck d) {
		return name.compareTo(d.name);
	}

	public static void main(String[] args) {

		// Remember: Numbers sort before letters & uppercase sort before
		// lowercase letters
		List<Duck> ducks = new ArrayList<>();
		ducks.add(new Duck("mike"));
		ducks.add(new Duck("Mike"));
		ducks.add(new Duck("Nancy"));
		ducks.add(new Duck("11"));
		ducks.add(new Duck("jonathan"));

		// Sort
		Collections.sort(ducks);

		System.out.println(ducks);
	}
}
