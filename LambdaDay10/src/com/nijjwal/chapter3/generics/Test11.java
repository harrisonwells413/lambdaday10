package com.nijjwal.chapter3.generics;

//Chapter 3 - Updating All Elements

import java.util.List;
import java.util.Arrays;

public class Test11{
	public static void main(String[] args){
		//Create a list
		List<Integer> list = Arrays.asList(1,2,3);
		
		//Print the list
		System.out.println("Numbers before applying replaceAll() method: " +list);
		
		//Remove using removeIf new method # 1
		list.replaceAll(x -> x*2);
		
		//Print the list again
		System.out.println("Numbers after applying replaceAll() method: " +list);
		
	}
}