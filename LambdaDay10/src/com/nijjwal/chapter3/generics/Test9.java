package com.nijjwal.chapter3.generics;

import java.util.Comparator;
import java.util.List;
import java.util.ArrayList;
import java.util.Collections;

class Duck9 {
	String name;
	int weight;

	public Duck9(String name, int weight) {
		this.name = name;
		this.weight = weight;
	}

	public String getName() {
		return name;
	}

	public int getweight() {
		return weight;
	}

	public String toString() {
		return name + " is " + weight + " pounds";
	}
}

class Duck9Helper {
	public static int compareByWeight(Duck9 d1, Duck9 d2) {
		return d1.getweight() - d2.getweight();
	}

	public static int compareByName(Duck9 d1, Duck9 d2) {
		return d1.getName().compareTo(d2.getName());
	}

	private Duck9Helper() {

	}
}

public class Test9 {
	public static void main(String[] args) {
		// Create comparator
		Comparator<Duck9> compareByWeight = Duck9Helper::compareByWeight;
		Comparator<Duck9> compareByName = Duck9Helper::compareByName;

		List<Duck9> listOfDuck9s = new ArrayList<>();
		listOfDuck9s.add(new Duck9("Alex", 20));
		listOfDuck9s.add(new Duck9("Cassie", 10));

		// Sort the Duck9s by weight
		Collections.sort(listOfDuck9s, compareByWeight);

		// Print all Duck9s by weight in natural order
		System.out.println("Sort by weight");
		System.out.println(listOfDuck9s);

		// Sort the Duck9s by name
		Collections.sort(listOfDuck9s, compareByName);

		// Print all Duck9s by name in natural order
		System.out.println("Sort by name");
		System.out.println(listOfDuck9s);

	}

}