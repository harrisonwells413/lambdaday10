/* Sample class to display that class cast exception can occur
 * when using Generics*/

package com.nijjwal.chapter3.generics;

import java.util.ArrayList;
import java.util.List;

public class Test3 {

	static void printNames(List list) {
		for (int i = 0; i < list.size(); i++) {
			String name = (String) list.get(i); // class case exception here
			System.out.println(name);
		}
	}

	public static void main(String... args) {
		List names = new ArrayList();
		names.add(new StringBuilder("Henry"));
		printNames(names);
	}

}